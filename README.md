Run the script, it will generate open a window that allows you to select 'shake level' and duration
If you click 'key animation' the selected camera will autokey 'handheld' frames on a new animation layer.
If you do not like the result, you have the option to delete it w/ a button click before closing the tool window.